Quick Repo Init Script
=============

To initialize resoitory with default .gitingnore file quickly execute suggested commands below.

```
robo init <client> <repo_url>
```

Where `robo` is a php task runner. Robo installation instructions can be found from here http://robo.li/#install

#### Available `client` are.
+ `iphone`
+ `android`
+ `web`