<?php
/**
 * Script file to initialize repository with basic files
 *
 * @package    InitRepo
 *
 * @author     Gunjan Patel <gunjan@tasolglobal.com>
 * @copyright  Copyright (C) 2008 - 2015 tasolglobal.com. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
class RoboFile extends \Robo\Tasks
{
	public function init($client, $repository)
	{
		/*git clone https://tasol@bitbucket.org/tasol/bubbles-android.git;
		cd bubbles-android; echo "Bubbles Android" > README.md; git add .; git ci -a -m "Adding Readme";
		git push origin master;*/
		$this->say("Processing $repository...");

		$this->taskDeleteDir($client)->run();
		$this->taskGitStack()
			->stopOnFail()
			->cloneRepo($repository, $client)
			->run();

		$this->taskExecStack()
			->stopOnFail()
			->exec('cp gitignore_' . $client . ' ' . $client . '/.gitignore')
			->run();

		$this->taskGitStack()
			->stopOnFail()
			->dir($client)
			->add('.gitignore')
			->commit('Initialize repo')
			->push('origin', 'master')
			->run();

		$this->taskDeleteDir($client)->run();
	}
}
